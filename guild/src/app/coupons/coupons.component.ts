import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.css']
})
export class CouponsComponent implements OnInit {

	coupons = [];
	constructor(private apiService: ApiService) { }
	ngOnInit() {
		let data = new FormData();
		this.apiService.published_deals(data).subscribe((data: any[])=>{  
			console.log(data);  
			this.coupons = data['data'];  
		})  
	}

}

