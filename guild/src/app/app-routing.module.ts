import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CouponsComponent } from './coupons/coupons.component';
import { AllDealsComponent } from './all-deals/all-deals.component';
import { ActiveDealsComponent } from './active-deals/active-deals.component';



const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'coupons', component: CouponsComponent },
  { path: 'all-deals', component: AllDealsComponent },
  { path: 'active-deals', component: ActiveDealsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
