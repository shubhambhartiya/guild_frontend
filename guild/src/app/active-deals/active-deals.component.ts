import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-active-deals',
  templateUrl: './active-deals.component.html',
  styleUrls: ['./active-deals.component.css']
})
export class ActiveDealsComponent implements OnInit {

	coupons = [];
	constructor(private apiService: ApiService,  private spinner: NgxSpinnerService) { }
	ngOnInit() {
		this.spinner.show();
		let data = new FormData();
		this.apiService.active_deals(data).subscribe((data: any[])=>{  
			console.log(data);  
			this.spinner.hide();
			this.coupons = data['data'];  
		})  
	}
}
