import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveDealsComponent } from './active-deals.component';

describe('ActiveDealsComponent', () => {
  let component: ActiveDealsComponent;
  let fixture: ComponentFixture<ActiveDealsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveDealsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
