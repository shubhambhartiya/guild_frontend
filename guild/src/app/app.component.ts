import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	title = 'guild';
	message_to_show = ""
	constructor(private apiService: ApiService) { }
	

	login_as_function(content){
		let data = new FormData();
		data.append("name", content)
		this.apiService.login_as(data).subscribe((data: any[])=>{  
			console.log(data);  
			localStorage.setItem('token', data['data']);
			this.message_to_show = "Successfully logged in as "+content
		})  
	}

	ngOnInit() {
		this.login_as_function("Super Admin")
	}

}

