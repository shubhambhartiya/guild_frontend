import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { CouponsComponent } from './coupons/coupons.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AllDealsComponent } from './all-deals/all-deals.component';
import { ActiveDealsComponent } from './active-deals/active-deals.component';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSpinnerModule } from "ngx-spinner";

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    CouponsComponent,
    AllDealsComponent,
    ActiveDealsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
