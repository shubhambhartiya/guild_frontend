import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpEvent, HttpErrorResponse, HttpEventType} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

	token = ""

  	url = 'http://guild.codezen.in'

	constructor(private _http: HttpClient) { }

	public published_deals(data):any{
	    this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders().set("Authorization" , "token " + this.token);
	    return this._http.post((this.url +'/coupons/get_published_deals/'), data, {headers:headers}, );
	}

	public active_deals(data):any{
	    this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders().set("Authorization" , "token " + this.token);
	    return this._http.post((this.url +'/coupons/get_active_deals/'), data, {headers:headers}, );
	}

	public all_deals(data):any{
	    this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders().set("Authorization" , "token " + this.token);
	    return this._http.post((this.url +'/coupons/get_all_deals/'), data, {headers:headers}, );
	}

	public save_and_publish_deal(data):any{
	    this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders().set("Authorization" , "token " + this.token);
	    return this._http.post((this.url +'/coupons/publish_deal/'), data, {headers:headers}, );
	}

	public start_edit_deal(data):any{
	    this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders().set("Authorization" , "token " + this.token);
	    return this._http.post((this.url +'/coupons/start_edit_deal/'), data, {headers:headers}, );
	}

	public edit_deal(data):any{
		this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders().set("Authorization" , "token " + this.token);
	    return this._http.post((this.url +'/coupons/edit_deal/'), data, {headers:headers}, );
	}

	public login_as(data):any{
		this.token = localStorage.getItem('token');
	    const headers = new HttpHeaders();
	    return this._http.post((this.url +'/users/login_as/'), data, {headers:headers}, );
	}

}