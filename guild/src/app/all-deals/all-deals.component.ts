import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import { NgxSpinnerService } from 'ngx-spinner';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-all-deals',
  templateUrl: './all-deals.component.html',
  styleUrls: ['./all-deals.component.css']
})
export class AllDealsComponent implements OnInit {

	coupons = [];
	all_coupons = [];
	api_message = ""
	notEmptyPost = true
	notscrolly = true
	page : any
	modal_message = ""
	selected_deal = ""
	new_name = ""
	description = ""

	closeResult: string;

	all_deals_list(){
		let data = new FormData();
		data.append("page", this.page)
		this.apiService.all_deals(data).subscribe((data: any[])=>{  
			console.log(data);  
			this.spinner.hide()
			this.coupons = data['data'];  

			if (this.coupons.length === 0 ) {
				this.notEmptyPost =  false;
			}
			this.all_coupons = this.all_coupons.concat(this.coupons);
			this.notscrolly = true;
		})  
	}

	constructor(private apiService: ApiService, private spinner: NgxSpinnerService, private modalService: NgbModal) { }
	ngOnInit() {
		this.page = 1
		this.all_deals_list()
	}

	onScroll() {
		if (this.notscrolly && this.notEmptyPost) {
			this.spinner.show();
			this.notscrolly = false;
			this.all_deals_list();
			this.page = this.page + 1
		}
	}


	saveAndPublish(pk){
        let data = new FormData();
        data.append("pk", pk)
        this.apiService.save_and_publish_deal(data).subscribe((res)=>{
			console.log(res)
			this.api_message = res.message[0]

			this.all_coupons.forEach(obj => {
				if(obj.pk == pk){
					if (obj.published){
						obj.published = false
						
					}else{
						obj.published = true
					}
				}
			});
			this.all_deals_list()
        })
    }


    start_edit_deal(x){
		let data = new FormData();
		data.append("pk", x.pk)
		this.apiService.start_edit_deal(data).subscribe((res)=>{  
			console.log(res);  
			if (!res.result){
				this.modal_message = res.message[0]
			}
		})  
	}

	callEditAPI(current_deal){
		console.log(this.new_name, this.description)
		let data = new FormData();
		data.append("pk", current_deal.pk)
		data.append("cLabel", this.new_name)
		data.append("cDescription", this.description)
		
		
		this.apiService.edit_deal(data).subscribe((res)=>{  
			console.log(res);  
			this.modal_message = res.message[0]
			this.all_deals_list()
			if (res.result){
				this.all_coupons.forEach(obj => {
					if(obj.pk == current_deal.pk){
						obj.cLabel = this.new_name
						obj.cDescription = this.description
					}
				});
			}
		})  
	}


    open(content, x) {
    	console.log(x)
    	this.selected_deal = x
    	this.start_edit_deal(x)
    	this.description = x.cDescription
		this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
		  this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
		  return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
		  return 'by clicking on a backdrop';
		} else {
		  return  `with: ${reason}`;
		}
	}
}
